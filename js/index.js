"use strict"

let items = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function itemList(arr, parent = document.body) {
    let ul = document.createElement('ul');

    arr.map(function (item) {
            let li = document.createElement('li');
            li.textContent = item;
            ul.append(li)
        }
    )
    if (parent !== document.body) {
        let parentList = document.createElement(parent);
        parentList.prepend(ul);
        document.body.prepend(parentList);
    } else {
        parent.prepend(ul);
    }
}

itemList(items, "div");

let timer;
let x = 3;
countdown();

function countdown() {
    console.log(x)
    x--;
    if (x < 0) {
        document.body.innerHTML = '';

    } else {
        timer = setTimeout(countdown, 1000);
    }
}

